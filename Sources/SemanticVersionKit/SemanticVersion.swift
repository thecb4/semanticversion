import Foundation

public struct SemanticVersion: Codable {
  var major: Int
  var minor: Int
  var patch: Int
  var preRelease: [String]
  var build: [String]

  static let malformed = SemanticVersion(
    major: -1,
    minor: 0,
    patch: 0,
    preRelease: [],
    build: []
  )
}

extension SemanticVersion {
  public var version: String {
    "\(major).\(minor).\(patch)"
  }

  public var preReleaseIdentifiers: String {
    preRelease.isEmpty ? "" : ("-" + preRelease.joined(separator: "."))
  }

  public var buildIdentifiers: String {
    build.isEmpty ? "" : ("+" + build.joined(separator: "."))
  }

}

extension SemanticVersion {
  public init?(json: String) {
    let decoder = JSONDecoder()

    guard let data = json.data(using: .utf8) else {
      return nil
    }

    guard let sm = try? decoder.decode(SemanticVersion.self, from: data) else {
      return nil
    }

    self = sm
  }

  var json: String? {
    let encoder = JSONEncoder()

    encoder.outputFormatting = [.prettyPrinted, .sortedKeys]

    guard let jsonData = try? encoder.encode(self) else { return nil }

    guard let jsonString = String(data: jsonData, encoding: .utf8) else {
      return nil
    }

    return jsonString
  }
}

extension SemanticVersion: ExpressibleByStringLiteral {

  public init(stringLiteral value: String) {
    self = SemanticVersion.Parser.parse(value)
  }

  public init(string: String) {
    self = .init(stringLiteral: string)
  }
}

extension SemanticVersion {
  enum Identifier: String, CaseIterable {
    case major
    case minor
    case patch
    case prerelease
    case buildmetadata
  }
}

extension SemanticVersion: CustomStringConvertible {
  public var description: String {
    version + preReleaseIdentifiers + buildIdentifiers
  }
}

extension SemanticVersion {
  struct Parser {
    static let preReleaseMarker: Character = "-"
    static let metadataMarker: Character = "+"
    static let commonSeparator: Character = "."

    static func parse(_ string: String) -> SemanticVersion {
      let prereleaseStartIndex = string.firstIndex(of: SemanticVersion.Parser.preReleaseMarker)
      let metadataStartIndex = string.firstIndex(of: SemanticVersion.Parser.metadataMarker)

      // stop after the major.minor.patch
      let versionCoreEndIndex = prereleaseStartIndex ?? metadataStartIndex ?? string.endIndex
      let versionCore = string.prefix(upTo: versionCoreEndIndex)
      var versionCoreComponents = versionCore.split(
        separator: SemanticVersion.Parser.commonSeparator
      ).map { String($0) }

      // allow for equivalence 10 == 10.0, 10.1 == 10.1.0
      if versionCoreComponents.count == 1 { versionCoreComponents += ["0", "0"] }
      if versionCoreComponents.count == 2 { versionCoreComponents += ["0"] }

      guard versionCoreComponents.count == 3 else { return SemanticVersion.malformed }

      guard let major = Int(versionCoreComponents[0]) else { return SemanticVersion.malformed }
      guard let minor = Int(versionCoreComponents[1]) else { return SemanticVersion.malformed }
      guard let patch = Int(versionCoreComponents[2]) else { return SemanticVersion.malformed }

      let prereleaseIdentifiers = identifiers(
        for: string, start: prereleaseStartIndex, end: metadataStartIndex ?? string.endIndex)

      // if you have an identifier but the capture is empty return malformed
      if prereleaseIdentifiers.count == 0 && prereleaseStartIndex != nil {
        return SemanticVersion.malformed
      }

      let metadataIdentifiers = identifiers(
        for: string, start: metadataStartIndex, end: string.endIndex)

      // if you have an identifier but the capture is empty return malformed
      if metadataIdentifiers.count == 0 && metadataStartIndex != nil {
        return SemanticVersion.malformed
      }

      return SemanticVersion(
        major: major,
        minor: minor,
        patch: patch,
        preRelease: prereleaseIdentifiers,
        build: metadataIdentifiers
      )
    }

    static func identifiers(for string: String, start: String.Index?, end: String.Index) -> [String]
    {
      guard let start = start else { return [] }
      let identifiers = string[string.index(after: start)..<end]
      return identifiers.split(separator: SemanticVersion.Parser.commonSeparator).map { String($0) }
    }
  }

}

extension SemanticVersion {
  mutating func bump(_ identifier: SemanticVersion.Identifier) {
    switch identifier {
      case .patch:
        self.patch += 1
      case .minor:
        self.patch = 0
        self.minor += 1
      case .major:
        self.patch = 0
        self.minor = 0
        self.major += 1
      default:
        return
    }
  }
}

extension SemanticVersion: Equatable {}
extension SemanticVersion: Hashable {}
