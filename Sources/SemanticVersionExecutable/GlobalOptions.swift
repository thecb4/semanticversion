import ArgumentParser

struct Options: ParsableArguments {

  @Argument(help: "The version file to update")
  var versionFile: String

}