import ArgumentParser
import Foundation
import SemanticVersionKit

@main
struct SwiftSemanticVersion: ParsableCommand {

  static var configuration = CommandConfiguration(
    abstract: "Manage the semantic version of a package",
    version: version,
    subcommands: [Init.self],
    defaultSubcommand: Init.self
  )

  static var version: String {
    guard let versionURL = Bundle.module.url(forResource: "product", withExtension: "version")
    else {
      return "No Version Provided"
    }

    guard let string = try? String(contentsOf: versionURL) else {
      return "No Version Provided"
    }

    let version = SemanticVersion(string: string)

    return version.description
  }

  // mutating func run() throws {
  //   print(SwiftSemanticVersion.version)

  //   print("Version File to be modified: \(versionFile)")
  // }
}