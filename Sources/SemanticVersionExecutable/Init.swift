import ArgumentParser
import Foundation
import SemanticVersionKit

extension SwiftSemanticVersion {
  struct Init: ParsableCommand {
    static var configuration =
      CommandConfiguration(abstract: "Initialized")

    // The `@OptionGroup` attribute includes the flags, options, and
    // arguments defined by another `ParsableArguments` type.
    @OptionGroup var options: Options

    mutating func run() {
      print("will create file \(options.versionFile)")
    }
  }
}