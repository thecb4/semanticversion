import XCTest

@testable import SemanticVersionKit

final class SemanticVersionTests: XCTestCase {
  func testCreateSemanticVersion() {
    // given
    let major = 1
    let minor = 0
    let patch = 0
    let preRelease: [String] = []
    let build: [String] = []

    // when
    let semVer = SemanticVersion(
      major: major,
      minor: minor,
      patch: patch,
      preRelease: preRelease,
      build: build
    )

    // then
    XCTAssertEqual(semVer.major, 1)
    XCTAssertEqual(semVer.minor, 0)
    XCTAssertEqual(semVer.patch, 0)
    XCTAssertEqual(semVer.preRelease, [])
    XCTAssertEqual(semVer.build, [])
  }

  func testStringRepresentation() {

    // given
    let semVer = SemanticVersion(
      major: 1,
      minor: 0,
      patch: 0,
      preRelease: ["beta"],
      build: ["12345", "Saturday"]
    )

    // when
    let description = semVer.description

    // then
    XCTAssertEqual(description, "1.0.0-beta+12345.Saturday")

  }

  func testWriteSemanticVersionJSON() throws {
    // given
    let semVer = SemanticVersion(
      major: 1,
      minor: 0,
      patch: 0,
      preRelease: ["beta"],
      build: ["12345", "Saturday"]
    )

    let expectedString =
      """
      {
        "build" : [
          "12345",
          "Saturday"
        ],
        "major" : 1,
        "minor" : 0,
        "patch" : 0,
        "preRelease" : [
          "beta"
        ]
      }
      """

    // when
    guard let jsonString = semVer.json else {
      XCTFail("Could not convert semantic version to json")
      return
    }

    // then
    XCTAssertEqual(jsonString, expectedString)
  }

  func testReadSemanticVersionJSON() throws {

    let string = try String(contentsOfFile: "Tests/Fixtures/version.json")

    guard let semVer = SemanticVersion(json: string) else {
      XCTFail("Could not create semantic version from json")
      return
    }

    // then
    XCTAssertEqual(semVer.major, 1)
    XCTAssertEqual(semVer.minor, 0)
    XCTAssertEqual(semVer.patch, 0)
    XCTAssertEqual(semVer.preRelease, ["beta"])
    XCTAssertEqual(semVer.build, ["12345", "Saturday"])
  }

  func testSemanticVersionFromString() throws {
    // given
    guard let versionURL = Bundle.module.url(forResource: "product", withExtension: "version")
    else {
      XCTFail("Couldn't get package.version")
      return
    }

    let string = try String(contentsOf: versionURL)

    // when
    let semVer = SemanticVersion(string: string)

    // then
    XCTAssertEqual(semVer.major, 1)
    XCTAssertEqual(semVer.minor, 0)
    XCTAssertEqual(semVer.patch, 0)
    XCTAssertEqual(semVer.preRelease, [])
    XCTAssertEqual(semVer.build, [])
  }

  func testExpressibleByStringLiteralComplete() {
    // given,when
    let semVer: SemanticVersion = "1.0.0-beta+12345.Saturday"

    // then
    XCTAssertEqual(semVer.major, 1)
    XCTAssertEqual(semVer.minor, 0)
    XCTAssertEqual(semVer.patch, 0)
    XCTAssertEqual(semVer.preRelease, ["beta"])
    XCTAssertEqual(semVer.build, ["12345", "Saturday"])
  }

  func testExpressibleByStringLiteralMajorOnly() {
    // given,when
    let semVer: SemanticVersion = "1"

    // then
    XCTAssertEqual(semVer.major, 1)
    XCTAssertEqual(semVer.minor, 0)
    XCTAssertEqual(semVer.patch, 0)
    XCTAssertEqual(semVer.preRelease, [])
    XCTAssertEqual(semVer.build, [])
  }

  func testExpressibleByStringLiteralMajorMinorOnly() {
    // given,when
    let semVer: SemanticVersion = "1.1"

    // then
    XCTAssertEqual(semVer.major, 1)
    XCTAssertEqual(semVer.minor, 1)
    XCTAssertEqual(semVer.patch, 0)
    XCTAssertEqual(semVer.preRelease, [])
    XCTAssertEqual(semVer.build, [])
  }

  func testBumpPatch() {
    // given
    var version: SemanticVersion = "1.0.0"

    // when
    version.bump(.patch)

    // then
    XCTAssertEqual(version.major, 1)
    XCTAssertEqual(version.minor, 0)
    XCTAssertEqual(version.patch, 1)
    XCTAssertEqual(version.preRelease, [])
    XCTAssertEqual(version.build, [])
  }

  func testBumpMinor() {
    // given
    var version: SemanticVersion = "1.0.0"

    // when
    version.bump(.minor)

    // then
    XCTAssertEqual(version.major, 1)
    XCTAssertEqual(version.minor, 1)
    XCTAssertEqual(version.patch, 0)
    XCTAssertEqual(version.preRelease, [])
    XCTAssertEqual(version.build, [])
  }

  func testBumpMajor() {
    // given
    var version: SemanticVersion = "1.0.0"

    // when
    version.bump(.major)

    // then
    XCTAssertEqual(version.major, 2)
    XCTAssertEqual(version.minor, 0)
    XCTAssertEqual(version.patch, 0)
    XCTAssertEqual(version.preRelease, [])
    XCTAssertEqual(version.build, [])
  }

  func testAccessPackageVersion() throws {

    // given
    guard let versionURL = Bundle.module.url(forResource: "product", withExtension: "version")
    else {
      XCTFail("Couldn't get product.version")
      return
    }

    let string = try String(contentsOf: versionURL)

    // when
    let version = SemanticVersion(string: string)

    // then
    XCTAssertEqual(version.major, 1)
    XCTAssertEqual(version.minor, 0)
    XCTAssertEqual(version.patch, 0)
    XCTAssertEqual(version.preRelease, [])
    XCTAssertEqual(version.build, [])
  }

  func testExpectMalformed() {
    // given,when
    let semVer: SemanticVersion = "1.B"

    // then
    XCTAssertEqual(semVer, SemanticVersion.malformed)
  }

  func testPartialEquivalence() {
    // given,when
    let v1: SemanticVersion = "1"
    let v2: SemanticVersion = "1.0"

    // then
    XCTAssertEqual(v1, v2)
  }

  func testBadIdentifiers() {
    // given,when
    let version: SemanticVersion = "1-+"

    // then
    XCTAssertEqual(version, SemanticVersion.malformed)
  }
}
